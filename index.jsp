
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>


<script type="text/javascript">
	var itemCount;
	$(document).ready(function(){
		fnList('list');   
	});

	function fnList(){
		var url="https://raw.githubusercontent.com/emtete/victor/master/ajaxExam/data.xml";
	     $.ajax({
	        type:"POST"         
			,url:url         
	        ,data: {}    
	     	,dataType:"xml"
	        ,success:function(items){

	        	itemCount = $(items).find("item").length;

	        	fnGenerateHtml(items);		//생성되는 태그는 가장 먼저 실행되야 한다.
	        	fnCheckboxEventBinding();	//체크박스 하나만 선택되도록.
				fnDelEventBinding();
	        	fnSwitchRowEventBinding();

	          }//success
		     ,error:function(e) {
		     }
	     });//ajax
	}//fnList

	function fnGenerateHtml(items){
		$(items).find("item").each(function() {
        	 var goodTime = $(this).find("goodTime").text(); 
        	 var hour = (parseInt(goodTime / 60) != 0) ? parseInt(goodTime / 60)+'시' : '';
    		 var minute = ( (goodTime % 60) !=0 ) ? ' '+ (goodTime % 60)+'분' :'간' ;
    		 var time = hour + minute;

		     var orderBy = $(this).find("orderBy").text();  
		     var goodId = $(this).find("goodId").text();  
		     var goodPrice = $(this).find("goodPrice").text();
		     var goodName = $(this).find("goodName").text();    
	     
		    $('#tbodyid tr:last').after(
	        			  '<tr>'
							+'<td class="chk"><input type="checkbox"/></td>'
							+'<td><a href="#" >'+goodName+'</a></td>'
							+'<td class="time">'+time+'</td>'
							+'<td class="price">'+goodPrice+'원</td>'
							+'<td class="del"><button type="button" name="deleteBtn">삭제</button></td>'
							+'<input type="hidden" name="goodId" value="'+goodId+'"/> '
							+'<input type="hidden" name="orderBy" value="'+orderBy+'"/> '
						+'</tr>'
	        );//after
	    });//each
	}//fnGenerateHtml

	//체크박스 한개만 선택되도록.
	function fnCheckboxEventBinding(){
		$("input:checkbox").on('click',function(){
			$("input:checked").not(this).attr('checked',false);
		});	     
	}

	function fnDelEventBinding(runSecondCode){ 
		if(!runSecondCode){
			$('button[name=deleteBtn]').each(function(e){
				$(this).off();
				$(this).on('click',function(){
					var deletedId = $(this).parent().nextAll('input[name=orderBy]').val();
					$('input[value='+deletedId+']').parent().remove();
					$('input[name=orderBy]').each(function(e){
						var orderBy = $(this).val();
						if( orderBy > deletedId && orderBy != 'empty'){
							$(this).val(orderBy - 1);
						}
					});//orderBy each
					itemCount -= 1;
					fnDelEventBinding(true);
				});//click
			});//each
		}else{
			$('button[name=deleteBtn]').each(function(e){
				$(this).off();
				$(this).on('click',function(){
					var deletedId = $(this).parent().nextAll('input[name=orderBy]').val();
					$('input[value='+deletedId+']').parent().remove();
					$('input[name=orderBy]').each(function(e){
						var orderBy = $(this).val();
						if( orderBy > deletedId && orderBy != 'empty'){
							$(this).val(orderBy - 1);
						}
					});
					itemCount -= 1;
				});//clickEvent
			});//Button each
		}//if
	}//fnDelEventBinding

	function fnSwitchRowEventBinding(){
		$("#upButton").on('click',function(){
			fnSwitchEvent('up');
		});//upButton
		
		$("#downButton").on('click',function(){
			fnSwitchEvent('down');
		});//downButton
	}

	function fnSwitchEvent(action){
		var orderBy = $("input:checkbox:checked").parent().nextAll('input[name=orderBy]').val();
		console.log('orderBy : '+orderBy);
			orderBy *= 1;
			if(orderBy > 1 && action == 'up'){
				$('input[value='+orderBy+']').prevAll('.del').children().off();
				$('input[value='+(orderBy*1-1)+']').prevAll('.del').children().off();
				fnCommonUpDown('up', orderBy);
				fnDelEventBinding(orderBy, orderBy*1-1);

			}else if(orderBy < itemCount && action == 'down' ){
				$('input[value='+orderBy+']').prevAll('.del').children().off();
				$('input[value='+(orderBy*1+1)+']').prevAll('.del').children().off();
				fnCommonUpDown('down', orderBy);
				fnDelEventBinding(orderBy, orderBy*1+1);

			}//if
	}
	
	function fnCommonUpDown(action, orderBy){
		var checked_tr = $("input:checkbox:checked").parent().parent();
		var checked_tr_orderBy = orderBy;
		var checked_tr_html = checked_tr.html();
		var target_tr;
		if(action == 'up'){
			 target_tr = checked_tr.prev();
		}else if(action == 'down'){
			 target_tr = checked_tr.next();
		}
		var target_tr_orderBy = target_tr.children('input[name=orderBy]').val();
		var target_tr_html = target_tr.html();
		var checked_good_id = checked_tr.children().nextAll("input[name=goodId]").val();
		var target_good_id = target_tr.children().nextAll("input[name=goodId]").val();
		
		fnUpDown(checked_tr, checked_tr_orderBy, target_tr, target_tr_orderBy, checked_good_id, target_good_id, checked_tr_html, target_tr_html);
	}//fnCommonUpDown
	
	function fnUpDown(checked_tr, checked_tr_orderBy, target_tr, target_tr_orderBy, checked_good_id, target_good_id, checked_tr_html, target_tr_html){
	
    	checked_tr.html(target_tr_html);
    	target_tr.html(checked_tr_html);


    	checked_tr.children('input[name=orderBy]').val(checked_tr_orderBy);
    	target_tr.children('input[name=orderBy]').val(target_tr_orderBy);

    	$('input[value='+target_tr_orderBy+']').prevAll("td.chk").children().attr('checked',true);
		
		checked_tr.attr('class','');
		target_tr.attr('class','on');
		
		fnCheckboxEventBinding();
	}//fnUpDown
</script>

<div id="wrap">
	<div id="content">
		<div class="content">
			<h2>상품메뉴</h2>
			<div class="section">
				<div class="tab clfix">
					
				</div>
				<div class="clfix mt20">
					<span class="t11 fl mt5">* 위/아래 버튼을 이용하여 상품메뉴 목록의 순서를 변경할 수 있습니다.</span>
					<div class="fr">
						<a href="javascript:;" class="btn und" id="upButton">▲ &nbsp;&nbsp; 위</a>
						<a href="javascript:;" class="btn und" id="downButton">▼ 아래</a>
					</div>
				</div>
				<div class="prod_list mt10">
					<form action="post" name="newGoodForm" id="newGoodForm">
						<table>
							<colgroup>
								<col width="53"/>
								<col width="330"/>
								<col width="85"/>
								<col width="85"/>
							</colgroup>
							<tbody id="tbodyid">
							<tr id="empty"></tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


